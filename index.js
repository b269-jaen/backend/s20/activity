
// function for the first loop
function firstLoop(){
	// prompt number from the user
	let number = prompt("Enter a number: ");

	console.log("The number you provided is " +number);

	// initialize loop
	for(let i = number; i > 0; i--){

		// if the number is less than 50, break the loop.
		if(i <= 50){
			console.log("The current value is at 50. Terminating the loop.")
			break;
		}

		// if the number is divisible by 10, skip number
		if(i % 10 === 0){
			console.log("The number is divisible by 10. Skipping the number.");
			continue;
			// if the number is divisible by 5, print number.
		}else if(i % 5===0){
				console.log(i);	
		}

	}

}

firstLoop();


// function for the second loop
function secondLoop(){
	// initialize string
	let word = 'supercalifragilisticexpialidocious';
	// print string
	console.log(word);

	// initialize array container
	let newWord = ['']

	// while i < word.length, execute loop
	for(let i = 0; i < word.length; i++){


		// vowel letters must be skipped 
		if( word[i] == "a" || 
			word[i] == "e" || 
			word[i] == "i" || 
			word[i] == "o" || 
			word[i] == "u" ){
		continue;
		// consonant letters will be stored in the array
		}else{
			newWord += word[i];
		}

	}
	// print array items
	console.log(newWord);
}
secondLoop();	